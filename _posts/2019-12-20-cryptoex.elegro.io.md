---
title: "elegro Exchange - Bitcoin and crypto exchange"
altTitle: 

users: 100
appId: cryptoex.elegro.io
launchDate: 
latestUpdate: 2020-03-10
apkVersionName: "1.0.2"
stars: 4.9
ratings: 106
reviews: 12
size: 11M
website: 
repository: 
issue: 
icon: cryptoex.elegro.io.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/cryptoex.elegro.io/
redirect_from:
  - /cryptoex.elegro.io/
---


This page was created by a script from the **appId** "cryptoex.elegro.io" and public
information found
[here](https://play.google.com/store/apps/details?id=cryptoex.elegro.io).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.