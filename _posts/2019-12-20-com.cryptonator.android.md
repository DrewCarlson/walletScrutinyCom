---
title: "Cryptonator cryptocurrency wallet"
altTitle: 

users: 100000
appId: com.cryptonator.android
launchDate: 2018-11-01
latestUpdate: 2020-01-17
apkVersionName: "3.0.1"
stars: 4.1
ratings: 4061
reviews: 2347
size: 8.6M
website: https://www.cryptonator.com/
repository: 
issue: 
icon: com.cryptonator.android.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2020-03-17
reviewStale: false
signer: 
reviewArchive:
- date: 2019-11-12
  version: "3.0.1"
  apkHash: 
  gitRevision: acb5634ce0405f12d9924759b045407fde297306
  verdict: nosource

providerTwitter: cryptonatorcom
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/2019/11/cryptonator/
redirect_from:
  - /cryptonator/
  - /com.cryptonator.android/
---


Cryptonator cryptocurrency wallet
makes no claim to be non-custodial but the
[Customer Support](https://www.cryptonator.com/contact/other/)
is pretty unambigously pointing towards it being custodial:

> **Do you provide private keys?**: No

Absent source code
we cannot verify it anyway. This wallet is **not verifiable**.
