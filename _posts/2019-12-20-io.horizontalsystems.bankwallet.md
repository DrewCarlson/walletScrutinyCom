---
title: "UNSTOPPABLE - Bitcoin Wallet"
altTitle: 

users: 1000
appId: io.horizontalsystems.bankwallet
launchDate: 2018-12-18
latestUpdate: 2020-06-05
apkVersionName: "0.14.0"
stars: 4.4
ratings: 169
reviews: 143
size: 28M
website: https://unstoppable.money/
repository: https://github.com/horizontalsystems/unstoppable-wallet-android
issue: https://github.com/horizontalsystems/unstoppable-wallet-android/issues/2326
icon: io.horizontalsystems.bankwallet.png
bugbounty: 
verdict: nonverifiable # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2020-06-07
reviewStale: false
signer: c1899493e440489178b8748851b72cbed50c282aaa8c03ae236a4652f8c4f27b
reviewArchive:
- date: 2020-05-16
  version: "0.13.0"
  apkHash: 20b023949e0572af577beb0df94c6dbaf758be0d7bd323e632392c93c6640f2d
  gitRevision: f2664abad8e41ce1b3e225be0eae63d18a0cc053
  verdict: verifiable
- date: 2020-03-25
  version: "0.12.0"
  apkHash: cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
  gitRevision: 45359b810b471200750ab0914f6c506054cf1123
  verdict: nonverifiable
- date: 2020-03-21
  version: "0.12.0"
  apkHash: cc616d5c4b67911e3ef65dc58f6cf045d18810826cd362a85776459607cb070c
  gitRevision: 65d19944379884fc3f0b9268e7e83b7dda63b5ba
  verdict: nonverifiable
- date: 2020-01-31
  version: "0.11.0"
  apkHash: f5bd6b218bb5e4fa605ed0c8e3dd9f424baf2656b9008f269d9e34697e0b21c0
  gitRevision: 43d012f4990ca6fed9d4b042224bf8fdd48ff41e
  verdict: verifiable
- date: 2020-01-29
  version: "0.11.0"
  apkHash: f5bd6b218bb5e4fa605ed0c8e3dd9f424baf2656b9008f269d9e34697e0b21c0
  gitRevision: 92e4a67ecc626220965114cd6a4cd67497e3be9f
  verdict: nonverifiable

providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/io.horizontalsystems.bankwallet/
redirect_from:
  - /io.horizontalsystems.bankwallet/
---


The latest version on Google Play did as follows with our (slightly modified)
[test script](https://gitlab.com/walletscrutiny/walletScrutinyCom/blob/master/test.sh):

```
Results for 
appId:          io.horizontalsystems.bankwallet
apkVersionName: 0.14.0
apkVersionCode: 27
apkHash:        9abbc4c1b7475c75437c416f5e103d4fd83625f0a7463be6aec545bff86d920d

Diff:
Files /tmp/fromPlay_io.horizontalsystems.bankwallet_27/apktool.yml and /tmp/fromBuild_io.horizontalsystems.bankwallet_27/apktool.yml differ
Files /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$6.smali and /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$6.smali differ
Files /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$7.smali and /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$7.smali differ
Files /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl.smali and /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl.smali differ
```

That is not good. Let's see the full diff:

```
$ diff --unified=0 -r /tmp/from*
diff '--unified=0' -r /tmp/fromBuild_io.horizontalsystems.bankwallet_27/apktool.yml /tmp/fromPlay_io.horizontalsystems.bankwallet_27/apktool.yml
--- /tmp/fromBuild_io.horizontalsystems.bankwallet_27/apktool.yml	2020-06-07 22:15:38.786607696 -0400
+++ /tmp/fromPlay_io.horizontalsystems.bankwallet_27/apktool.yml	2020-06-07 22:08:54.618341670 -0400
@@ -2 +2 @@
-apkFileName: app-release-unsigned.apk
+apkFileName: Unstoppable 0.14.0 (io.horizontalsystems.bankwallet).apk
diff '--unified=0' -r /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$6.smali /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$6.smali
--- /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$6.smali	2020-06-07 22:15:37.906606771 -0400
+++ /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$6.smali	2020-06-07 22:08:53.654341529 -0400
@@ -39 +39 @@
-    .line 306
+    .line 305
@@ -61 +61 @@
-    .line 309
+    .line 308
@@ -81 +81 @@
-    .line 311
+    .line 310
@@ -88 +88 @@
-    .line 312
+    .line 311
@@ -95 +95 @@
-    .line 313
+    .line 312
@@ -102 +102 @@
-    .line 314
+    .line 313
@@ -109 +109 @@
-    .line 315
+    .line 314
@@ -114 +114 @@
-    .line 317
+    .line 316
@@ -121 +121 @@
-    .line 319
+    .line 318
@@ -126 +126 @@
-    .line 321
+    .line 320
@@ -131 +131 @@
-    .line 324
+    .line 323
@@ -136 +136 @@
-    .line 325
+    .line 324
@@ -147 +147 @@
-    .line 327
+    .line 326
@@ -152 +152 @@
-    .line 330
+    .line 329
@@ -164 +164 @@
-    .line 332
+    .line 331
@@ -176 +176 @@
-    .line 341
+    .line 340
@@ -181 +181 @@
-    .line 337
+    .line 336
@@ -215 +215 @@
-    .line 341
+    .line 340
@@ -218 +218 @@
-    .line 342
+    .line 341
@@ -230 +230 @@
-    .line 306
+    .line 305
@@ -241 +241 @@
-    .line 347
+    .line 346
diff '--unified=0' -r /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$7.smali /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$7.smali
--- /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$7.smali	2020-06-07 22:15:37.906606771 -0400
+++ /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl$7.smali	2020-06-07 22:08:53.654341529 -0400
@@ -40 +40 @@
-    .line 362
+    .line 361
@@ -60 +60 @@
-    .line 362
+    .line 361
@@ -87 +87 @@
-    .line 365
+    .line 364
@@ -107 +107 @@
-    .line 367
+    .line 366
@@ -114 +114 @@
-    .line 368
+    .line 367
@@ -121 +121 @@
-    .line 369
+    .line 368
@@ -128 +128 @@
-    .line 370
+    .line 369
@@ -135 +135 @@
-    .line 371
+    .line 370
@@ -140 +140 @@
-    .line 372
+    .line 371
@@ -149 +149 @@
-    .line 373
+    .line 372
@@ -157 +157 @@
-    .line 376
+    .line 375
@@ -162 +162 @@
-    .line 378
+    .line 377
@@ -167 +167 @@
-    .line 381
+    .line 380
@@ -172 +172 @@
-    .line 382
+    .line 381
@@ -183 +183 @@
-    .line 384
+    .line 383
@@ -188 +188 @@
-    .line 387
+    .line 386
@@ -204 +204 @@
-    .line 389
+    .line 388
@@ -212 +212 @@
-    .line 390
+    .line 389
@@ -219 +219 @@
-    .line 397
+    .line 396
@@ -230 +230 @@
-    .line 398
+    .line 397
@@ -237 +237 @@
-    .line 403
+    .line 402
diff '--unified=0' -r /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl.smali /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl.smali
--- /tmp/fromBuild_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl.smali	2020-06-07 22:15:37.906606771 -0400
+++ /tmp/fromPlay_io.horizontalsystems.bankwallet_27/smali_classes2/io/horizontalsystems/bankwallet/core/storage/RatesDao_Impl.smali	2020-06-07 22:08:53.654341529 -0400
@@ -564 +564 @@
-    .line 291
+    .line 290
@@ -573 +573 @@
-    .line 294
+    .line 293
@@ -578 +578 @@
-    .line 296
+    .line 295
@@ -587 +587 @@
-    .line 300
+    .line 299
@@ -592 +592 @@
-    .line 302
+    .line 301
@@ -596 +596 @@
-    .line 305
+    .line 304
@@ -600 +600 @@
-    .line 306
+    .line 305
@@ -630 +630 @@
-    .line 355
+    .line 354
@@ -637 +637 @@
-    .line 358
+    .line 357
@@ -642 +642 @@
-    .line 360
+    .line 359
@@ -646 +646 @@
-    .line 362
+    .line 361
```

So it looks harmless (it should [only affect debug
messages](https://stackoverflow.com/questions/18274031/what-does-line-mean-in-smali-code-syntax-android-smali-code))
but it is a clear diff which has to be figured out where it
came from. It might just be an extra empty line in `RatesDao.kt` but to stay
consistent, we have to consider this result as **not verifiable**.
