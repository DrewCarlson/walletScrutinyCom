---
title: "BitWallet - Buy & Sell Bitcoin"
altTitle: 

users: 5000
appId: com.Android.Inc.bitwallet
launchDate: 
latestUpdate: 2020-06-05
apkVersionName: "1.4.6"
stars: 4.4
ratings: 256
reviews: 232
size: 25M
website: 
repository: 
issue: 
icon: com.Android.Inc.bitwallet.jpg
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.Android.Inc.bitwallet/
redirect_from:
  - /com.Android.Inc.bitwallet/
---


This page was created by a script from the **appId** "com.Android.Inc.bitwallet" and public
information found
[here](https://play.google.com/store/apps/details?id=com.Android.Inc.bitwallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.