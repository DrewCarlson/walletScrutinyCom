---
title: "TenX - Buy Bitcoin & Crypto Card"
altTitle: 

users: 100000
appId: com.onebit.app
launchDate: 2017-11-23
latestUpdate: 2020-06-03
apkVersionName: "3.20.0"
stars: 4.4
ratings: 754
reviews: 417
size: 84M
website: https://www.tenx.tech/
repository: 
issue: 
icon: com.onebit.app.png
bugbounty: 
verdict: custodial # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2019-12-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.onebit.app/
redirect_from:
  - /com.onebit.app/
---


This app's description hints at being custodial with

> Send cryptocurrencies to other TenX Wallet users for free

as there is no transaction fee if the transaction is just an update of their
centralized database.

As the core product of TenX is to spend your Bitcoins via a credit card, they
probably want to be in control of the coins and not surrender this control to
the user. We assume it is custodial.

Our verdict: **not verifiable**.
