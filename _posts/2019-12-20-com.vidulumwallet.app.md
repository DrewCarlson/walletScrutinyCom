---
title: "Vidulum - Multi-Asset Cryptocurrency Wallet"
altTitle: 

users: 500
appId: com.vidulumwallet.app
launchDate: 
latestUpdate: 2019-04-05
apkVersionName: "1.2"
stars: 4.7
ratings: 52
reviews: 42
size: 174k
website: 
repository: 
issue: 
icon: com.vidulumwallet.app.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2020-04-21
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.vidulumwallet.app/
redirect_from:
  - /com.vidulumwallet.app/
---


This page was created by a script from the **appId** "com.vidulumwallet.app" and public
information found
[here](https://play.google.com/store/apps/details?id=com.vidulumwallet.app).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.