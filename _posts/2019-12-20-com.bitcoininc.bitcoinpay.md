---
title: "Bitcoin Pay"
altTitle: 

users: 10000
appId: com.bitcoininc.bitcoinpay
launchDate: 
latestUpdate: 2017-07-06
apkVersionName: "1.0.1"
stars: 3.4
ratings: 82
reviews: 44
size: 14M
website: 
repository: 
issue: 
icon: com.bitcoininc.bitcoinpay.png
bugbounty: 
verdict: wip # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.bitcoininc.bitcoinpay/
redirect_from:
  - /com.bitcoininc.bitcoinpay/
---


This page was created by a script from the **appId** "com.bitcoininc.bitcoinpay" and public
information found
[here](https://play.google.com/store/apps/details?id=com.bitcoininc.bitcoinpay).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.