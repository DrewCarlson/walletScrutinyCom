---
title: "Changelly: Instant Bitcoin and Crypto Exchange"
altTitle: 

users: 50000
appId: com.changelly.app
launchDate: 
latestUpdate: 2020-04-17
apkVersionName: "2.4.3"
stars: 4.6
ratings: 725
reviews: 430
size: 12M
website: 
repository: 
issue: 
icon: com.changelly.app.png
bugbounty: 
verdict: nowallet # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2020-05-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/com.changelly.app/
redirect_from:
  - /com.changelly.app/
---


This app has no wallet feature in the sense that you hold Bitcoins in the app.