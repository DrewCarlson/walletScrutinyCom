---
title: "Pungo App"
altTitle: 

users: 500
appId: cloud.peer2.pungo_wallet
launchDate: 
latestUpdate: 2020-05-28
apkVersionName: "1.15"
stars: 0.0
ratings: 
reviews: 
size: 5.8M
website: 
repository: 
issue: 
icon: cloud.peer2.pungo_wallet.png
bugbounty: 
verdict: fewusers # May be any of: wip, fewusers, nowallet, nobtc, custodial, nosource, nonverifiable, verifiable, bounty, defunct
date: 2020-04-07
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

permalink: /posts/cloud.peer2.pungo_wallet/
redirect_from:
  - /cloud.peer2.pungo_wallet/
---


This page was created by a script from the **appId** "cloud.peer2.pungo_wallet" and public
information found
[here](https://play.google.com/store/apps/details?id=cloud.peer2.pungo_wallet).

Probably an engineer will soon have a deeper look at this app.

So far we are not even sure it is a wallet ... Please check back later.